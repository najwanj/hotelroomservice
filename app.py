import urllib
import json
import os

from flask import Flask
from flask import request
from flask import make_response
app = Flask(__name__)


@app.route('/webhook', methods=['POST'])
def webhook():
    req = request.get_json(silent=True, force=True)
    print("Request:")
    print(json.dumps(req, indent=4))
    res = makeWebhookResult(req)
    res = json.dumps(res, indent=4)
    print(res)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r

def makeWebhookResult(req):
    if req.get("result").get("action") == "liste":

       result = req.get("result")
       parameters = result.get("parameters")
       type1 = parameters.get("Repas_type")
       Repas = {'moderne':'Lasagne,Tacos,Pizza','traditionnel':'Poulet basique, Brochette,Couscous'}
       speech = "La liste des repas " + type1 + " est: " + Repas[type1]
       print("Response:")
       print(speech)
       return {
        "speech": speech,
        "displayText": speech,
        "source": "app"
       }
    elif req.get("result").get("action") == "list":
        result = req.get("result")
        parameters = result.get("parameters")
        type2 = parameters.get("Magazine_type")
        Magazine = {'feminin': 'elle, madmoizelle', 'psycho': 'PSYCHOLOGIeS, Cerveauetpsycho'}
        speech2 = "La liste des Magazines " + type2 + " est: " + Magazine[type2]
        print("Response:")
        print(speech2)
        return {
            "speech": speech2,
            "displayText": speech2,
            "source": "app"
        }
    elif req.get("result").get("action") == "dispo":
        result = req.get("result")
        parameters = result.get("parameters")
        type3 = parameters.get("Lieux_dispo")
        Lieux = {'ouverts': 'Jardin, Hall', 'fermes': 'Restaurant'}
        speech3 = "Les lieux " + type3 + " dispo pour prendre votre repas sont: " + Lieux[type3]
        print("Response:")
        print(speech3)
        return {
            "speech": speech3,
            "displayText": speech3,
            "source": "app"
        }

if __name__ == '__main__':
    port = int(os.getenv('PORT', 80))

    print ("Starting app on port %d" %(port))

    app.run(debug=True, port=port, host='0.0.0.0')
