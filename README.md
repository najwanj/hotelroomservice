Hotel Room Service chatbot vous permet de consulter :

1- Liste repas par type (traditionnel/moderne)

2- Liste magazine par type (feminin/psycho)

3- Lieux dispo pour prendre un repas par type (ouverts/fermes)

Hotel Room Service chatbot allows you to consult: 

1- List of meals by type (traditional / modern) 

2- Magazine list by type (female / psycho) 

3- Places available to have a meal by type (open / closed)

URL to access the Demo : https://bot.dialogflow.com/43b00f74-c788-41f9-b48e-bf42abc867e7